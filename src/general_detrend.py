#    Copyright 2020 Mykhailo Mishchenko
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


import numpy as np
import scipy.signal as sig

def general_detrend(detrend_type):
    '''
    Returns a detrend function of type `detrend_type` compatible with
    NaNs in data.
    '''
    def detrend(a):
                mask = np.isnan(a).any(axis=-1)
                result = np.empty_like(a)
                result[ mask] = np.nan
                result[~mask] = sig.detrend(a[~mask], type=detrend_type)
                return result
    return detrend
    
