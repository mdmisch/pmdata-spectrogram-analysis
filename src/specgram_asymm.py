#    Copyright 2020 Mykhailo Mishchenko
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.



from scipy.stats import moment
import pandas as pd
from general_detrend import general_detrend

def stat_characteristics(name, sample, calculate_mean=True, serie_base=None):
    chars = dict()
    
    if calculate_mean:
        chars['mean'] = np.mean(sample)
    chars['dispersion'] = moment(sample, 2)
    chars['asymmetry']  = moment(sample, 3) / chars['dispersion']**(3/2)
    chars['kurtosis']   = moment(sample, 4) / chars['dispersion']**2
    
    row = pd.Series(chars, dtype='object')
    if serie_base is not None:
        row = pd.Series(serie_base).append(row)
    row.name = name
    
    return row

if __name__ == '__main__':
    
    import argparse as ap
    from pathlib import Path
    from table_loader import fitbit_loader_types,             \
                             SliceTimeRangeProxyFitbitLoader, \
                             TableNotFound
    
    parser = ap.ArgumentParser()
    parser.add_argument('path', help='input files location',
                                type = Path,
                                nargs='?',
                                default=Path('./')          )
    parser.add_argument('--date-from', '-f',
                                       help='slice from this date',
                                       default=None          )
    parser.add_argument('--date-to'  , '-t',
                                       help='slice until this date',
                                       default=None          )
    parser.add_argument('--input-type', '-T',
                        help='type of data source; one of "{}"' \
                             .format('", "'.join(fitbit_loader_types.keys())),
                        default='hdf5',
                        choices=fitbit_loader_types.keys()
                       )
    parser.add_argument('--plot-type', '-p',
                        help='type of plot to build',
                        default='histogram',
                        choices=('histogram', 'scatter3d', 'scatter', 'pca_histogram', 'pca_scatter3d',
                                 'pca_spectrogram', 'pca', 'pca_mean_dir', 'pca_scatter',
                                 'pca_components')
                       )
    parser.add_argument('--just-load', '-L',
                        help  ='Load data and do nothing. Intended for usage with "python -i".',
                        action='store_const',
                        const=None,
                        dest='plot_type'
                       )
    parser.add_argument('--nperseg',
                        help   ='heart rate spectrogram window',
                        default=16,
                        type=int
                       )
    parser.add_argument('--noverlap',
                        help   ='heart rate spectrogram window overlap',
                        type=int
                       )
    parser.add_argument('--scaling', '-S',
                        help='scaling in scipy.signal.spectrogram',
                        default='spectrum'
                       )
    parser.add_argument('--log', '-l',
                        help='apply logarithm or other scaling',
                        choices=('e', '10', '2', 'sqrt'),
                        default='2'
                       )
    parser.add_argument('--bins', '-b',
                        help='histogram bins',
                        type=int,
                        default=1000
                       )
    parser.add_argument('-x',
                        help='scatter plot x axis',
                        type=int,
                        default=1
                       )
    parser.add_argument('-y',
                        help='scatter plot y axis',
                        type=int,
                        default=8
                       )
    parser.add_argument('-z',
                        help='scatter plot z axis',
                        type=int,
                        default=4
                       )
    parser.add_argument('--pc',
                        help='compute probability characteristics',
                        action='store_true',
                        default=True
                       )
    parser.add_argument('--pca-pc',
                        help='compute probability characteristics of PCA results',
                        action='store_true',
                        default=True
                       )
    parser.add_argument('--pca-on-spectrum',
                        help='compute PCA only on spectrum, excluding means (this is the default)',
                        action='store_true',
                        default=True
                       )
    parser.add_argument('--no-pca-on-spectrum',
                        help='compute PCA on spectrum and means',
                        action='store_false',
                        dest='pca_on_spectrum'
                       )
    parser.add_argument('--no-pc',
                        help='don\'t compute probability characteristics',
                        action='store_false',
                        dest='pc'
                       )
    parser.add_argument('--whiten',
                        help='whiten PCA',
                        action='store_true',
                        default=False
                       )
    parser.add_argument('--cmap',
                        default='seismic'
                       )
    parser.add_argument('--bucket-len',
                        help='length to upsample heartrate in minutes',
                        type=int,
                        default=5
                       )
    parser.add_argument('-d', '--detrend',
                        help='detrend alg.',
                        default='constant'
                       )
    parser.add_argument('-c', '--color',
                        help='color used for scatter plots',
                        default='#0000ff10'
                       )
    parser.add_argument('--hist-to-plot', 
                        help='plot only histograms corresponding to this numbers'
                             ' (numeration starts from zero)',
                        default=None,
                        nargs='*',
                        type=int
                        )
    parser.add_argument('--pca-components-other',
                        help='plot vectors for component 1 and 2 in \'pca-components\' plot type',
                        action='store_true'
                        )
    
    args = parser.parse_args()
    
    loader = fitbit_loader_types[args.input_type](args.path)
    # add slicing by time range
    loader = SliceTimeRangeProxyFitbitLoader(loader,
                                             args.date_from,
                                             args.date_to    )
    
    heart_rate = loader.get_table('heart_rate')
    
    ###############################################################
    
    from scipy.stats import moment
    import matplotlib.pyplot as plt
    import scipy.signal as sig
    import numpy as np
    
    np.set_printoptions(threshold=np.inf)
    
    hr_5min = heart_rate.set_index('dateTime') \
                        .resample('{}min'.format(args.bucket_len))      \
                        .mean()
    noverlap = args.noverlap or args.nperseg - 1
    
    if args.detrend == 'constant':
        detrend = 'constant'
    else:
        detrend = general_detrend(args.detrend)
    
    specgram = sig.spectrogram(hr_5min['value.bpm'], fs=1/(60*args.bucket_len),
                               nperseg=args.nperseg,
                               noverlap=noverlap,
                               scaling=args.scaling,
                               detrend=detrend,
                               window='boxcar')
    
    if args.log is None:
        specgram_scaled = specgram[-1]
    elif args.log == 'e':
        specgram_scaled = np.log(specgram[-1])
    elif args.log == '10':
        specgram_scaled = np.log10(specgram[-1])
    elif args.log == '2':
        specgram_scaled = np.log2(specgram[-1])
    elif args.log == 'sqrt':
        specgram_scaled = specgram[-1]**0.5
    
    specgram_cleaned = specgram_scaled[:,~np.isnan(specgram_scaled[0])]
    if args.pc:
        specgram_cleaned_char = pd.DataFrame(
            (stat_characteristics(i, row, serie_base={'freq':freq}) \
                for i, (freq, row) in enumerate(zip(specgram[0], specgram_cleaned))
            )
        )
        print('Spectrogram\'s rows statistic characteristics:')
        print(specgram_cleaned_char)
    
    if args.plot_type is not None and args.plot_type[:3] == 'pca':
        from sklearn.decomposition import PCA
        
        if args.pca_on_spectrum:
            to_pca = specgram_cleaned[1:].T
        else:
            to_pca = specgram_cleaned.T
            to_pca = to_pca[~np.isinf(to_pca).any(axis=1),:]
        
        pca = PCA(svd_solver='full', whiten=args.whiten)
        specgram_cleaned_pca = pca.fit_transform(to_pca)
        
        print('Means:')
        print(repr(pca.mean_))
        print('W:')
        print(repr(pca.components_))
        print()
        if args.pca_pc:
            pca_char = pd.DataFrame(
                (stat_characteristics(i, row, calculate_mean=False,
                                              serie_base=pd.Series((i,),
                                                                   index=('component',),
                                                                   dtype=int)
                                      ) \
                    for i, row in enumerate(specgram_cleaned_pca.T)
                )
            ).set_index('component')
            
            print('Spectrogram\'s PCA statistic characteristics:')
            print(pca_char)
        
    
    if args.plot_type == 'histogram':
        
        if args.hist_to_plot is not None:
            hist_to_plot = args.hist_to_plot
        else:
            hist_to_plot = range(1, len(specgram_cleaned))
        
        fig, axs = plt.subplots(len(hist_to_plot), 1, sharex=True, sharey=True)
        
        for hz, ax, row, in zip(specgram[0][hist_to_plot], axs, specgram_cleaned[hist_to_plot]):
            ax.hist(row, bins=args.bins, label='{:.6f} Hz'.format(hz))
            ax.legend(loc='upper left')
        
        plt.show()
    elif args.plot_type == 'scatter3d':
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        
        ax.scatter(specgram_scaled[args.x], specgram_scaled[args.y], specgram_scaled[args.z],
                   c=args.color)
        
        ax.set_xlabel('{:.6f} Hz'.format(specgram[0][args.x]))
        ax.set_ylabel('{:.6f} Hz'.format(specgram[0][args.y]))
        ax.set_zlabel('{:.6f} Hz'.format(specgram[0][args.z]))
        
        plt.show()
    elif args.plot_type == 'scatter':
        fig = plt.figure()
        ax = fig.add_subplot(111, aspect='equal')
        
        ax.scatter(specgram_scaled[args.x], specgram_scaled[args.y],
                   c=args.color)
        
        ax.set_xlabel('{:.6f} Hz'.format(specgram[0][args.x]))
        ax.set_ylabel('{:.6f} Hz'.format(specgram[0][args.y]))
        
        plt.show()
    elif args.plot_type == 'pca_histogram':
        if args.hist_to_plot is not None:
            hist_to_plot = args.hist_to_plot
        else:
            hist_to_plot = range(len(specgram_cleaned_pca.shape[1]))
        
        fig, axs = plt.subplots(len(hist_to_plot),#+int(args.pca_on_spectrum),
                                1, sharex=True, sharey=True)
        axs_iter = iter(axs)
        
        # if args.pca_on_spectrum:
            # try:
                # ax = next(axs_iter)
                # ax.hist(specgram_cleaned[0], bins=args.bins, label='specgram[0]')
                # ax.legend(loc='upper right')
            # except ValueError as e:
                # print(e)
        
        for i, ax, row in zip(hist_to_plot, axs_iter, specgram_cleaned_pca[:,hist_to_plot].T):
            ax.hist(row, bins=args.bins, label='pca[{}]'.format(i))
            ax.legend(loc='upper right')
        
        plt.show()
    elif args.plot_type == 'pca_scatter3d':
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        
        ax.scatter(specgram_cleaned_pca.T[args.x], specgram_cleaned_pca.T[args.y], specgram_cleaned_pca.T[args.z],
                   c=args.color)
        
        ax.set_xlabel('Component {}'.format(args.x))
        ax.set_ylabel('Component {}'.format(args.y))
        ax.set_zlabel('Component {}'.format(args.z))
        
        plt.show()
    elif args.plot_type == 'pca_scatter':
        fig = plt.figure()
        ax = fig.add_subplot(111, aspect='equal')
        
        ax.scatter(specgram_cleaned_pca.T[args.x], specgram_cleaned_pca.T[args.y],
                   c=args.color)
        
        ax.set_xlabel('Component {}'.format(args.x))
        ax.set_ylabel('Component {}'.format(args.y))
        
        plt.show()
    elif args.plot_type == 'pca_spectrogram':
        specgram_uncleaned_pca = np.full_like(specgram_scaled, np.nan)
        specgram_uncleaned_pca[:,~np.isnan(specgram_scaled[0])] = specgram_cleaned_pca.T
        
        sigma = moment(specgram_cleaned_pca.ravel(), 2) ** 0.5
        
        plt.imshow(specgram_uncleaned_pca, aspect='auto', cmap=args.cmap,
                   vmax=3*sigma, vmin=-3*sigma)
        plt.show()
    elif args.plot_type == 'pca_mean_dir':
        fig, axs = plt.subplots(1,1)
        if args.log is None:
            axs.set_yscale('log')
        axs.plot(specgram[0][1:], pca.mean_)
        axs.set_title('mean spectrum density')
        
        fig2, axs2 = plt.subplots(1,1)
        axs2.plot(specgram[0][1:], pca.components_[0])
        if args.log is None:
            axs2.set_yscale('log')
        axs2.set_title('PCA spectrum density direction')
        
        # fig3, axs3 = plt.subplots(1,1)
        # axs3.plot(specgram[0][1:], pca.components_[0]/pca.mean_)
        # axs3.set_yscale('log')
        # axs3.set_title('PCA spectrum density direction / mean spectrum density')
        
        plt.show()
    elif args.plot_type == 'pca_components':
        from solvers import nearest_linear_vector
        
        if args.whiten:
            print('Warning: whitening between two PCAs is a nonsence! Don\'t whiten!')
        
        k, b, w0_approx = nearest_linear_vector(pca.components_[0])
        
        print('w0_approx k:', k)
        print('w0_approx b:', b)
        print('w0_approx:')
        print(w0_approx)
        
        c0 = ((to_pca-pca.mean_[None,:]) @ w0_approx)
        to_pca2 = to_pca - c0[:,None] @ w0_approx[None,:]
        pca2 = PCA(svd_solver='full', whiten=args.whiten, n_components=to_pca.shape[1]-1)
        specgram_cleaned_pca2 = pca2.fit_transform(to_pca2)
         
        print('W[1:] approx:')
        print(repr(pca2.components_))
        print()
        
        pca2_chars = pd.DataFrame(
            (stat_characteristics(0, c0, calculate_mean=False,
                                         serie_base=pd.Series((0,),
                                                              index=('component',),
                                                              dtype=int)
                                 ),
            )
        )
        
        pca2_chars = pca2_chars.append(
            pd.DataFrame(
                (stat_characteristics(i, row, calculate_mean=False,
                                              serie_base=pd.Series((i,),
                                                                   index=('component',),
                                                                   dtype=int)
                                     ) \
                    for i, row in enumerate(specgram_cleaned_pca2.T, 1)
                )
            )
        )
        
        pca2_char = pca2_chars.set_index('component')
        
        print('Fixed PCA component\'s statistic characteristics:')
        print(pca2_char)
        
        if args.pca_on_spectrum:
            freqs = specgram[0][1:]
        else:
            freqs = specgram[0]
        
        fig, axs = plt.subplots(1,1, sharex=True, sharey=False)
        axs.plot(freqs, pca.components_[0], '+-', label='Component 0')
        axs.plot(freqs, w0_approx,          '+-', label='Component 0 approx.')
        if args.pca_components_other:
            axs.plot(freqs, pca .components_[1][:], '+-', label='Component 1')
            axs.plot(freqs, pca2.components_[0][:], '+-', label='Component 1 fixed')
            axs.plot(freqs, pca .components_[2][:], '+-', label='Component 2')
            axs.plot(freqs, pca2.components_[1][:], '+-', label='Component 2 fixed')
        axs.set_xlabel('Hz')
        axs.legend()
        
        char_fig, char_axs = plt.subplots(3,1)
        pca_char ['dispersion'].plot(ax=char_axs[0], style='+-', label='Dispercion (PCA)')
        pca2_char['dispersion'].plot(ax=char_axs[0], style='+-', label='Dispercion (fixed PCA)')
        char_axs[0].legend()
        
        pca_char ['asymmetry'].abs() .plot(ax=char_axs[1], style='+-', label='Abs. asymmetry (PCA)')
        pca2_char['asymmetry'].abs() .plot(ax=char_axs[1], style='+-', label='Abs. asymmetry (fixed PCA)')
        char_axs[1].legend()
        
        pca_char ['kurtosis']  .plot(ax=char_axs[2], style='+-', label='Kurtosis (PCA)')
        pca2_char['kurtosis']  .plot(ax=char_axs[2], style='+-', label='Kurtosis (fixed PCA)')
        char_axs[2].legend()
        
        plt.show()

