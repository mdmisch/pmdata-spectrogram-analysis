#    Copyright 2020 Mykhailo Mishchenko
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np

def nearest_linear_vector(a):
    a = np.asarray(a)
    n = len(a)
    
    s   =  a.sum()
    si  = (a*np.arange(n)).sum()
    
    isq = (np.arange(n)**2).sum()
    
    alpha = n*((n-1)*s - 2*si)/(n*(n-1)*si-2*s*isq)
    
    b = 1/( isq * alpha**2 +alpha*n*(n-1) + n)**0.5
    k = alpha*b
    x = k*np.arange(n)+b
    
    if np.dot(a,x) < 0:
        b = -b
        k = -k
        x = -x
    
    return k, b, x
