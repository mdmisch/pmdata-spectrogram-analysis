#    Copyright 2020 Mykhailo Mishchenko
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


from abc import abstractmethod, ABCMeta
from itertools import count
import html

class Node(metaclass=ABCMeta):
    def __new__(cls, example):
        if cls is Node:
            if type(example) is list:
                return ArrayNode(example)
            elif type(example) is dict:
                return DictNode(example)
            else:
                raise TypeError('`example` must be a `dict` or `list`, '
                                'but {} is given'.format(str(example)))
        else:
            return super().__new__(cls)
    
    # @abstractmethod
    # def comparison_recursion(self, example, depth_callback):
        # pass
    
    @abstractmethod
    def match(self, example):
        pass
    
    @abstractmethod
    def populate(self, example):
        pass

class NotMatchException(Exception):
    pass

class DictNode(Node):
    def __init__(self, example):
        self.attrs = dict()
        for key, val in example.items():
            try:
                val_repr = Node(val)
            except TypeError:
                val_repr = None
            self.attrs[key] = val_repr
    
    def comparison_recursion(self, example, depth_callback):
        '''
            For each (generic or non-generic) value pattern 
            `patternval` in `self` and corresponding value `val` in 
            `example` call `depth_callback(patternval, val)`.
            
            Stop comparison and raise `NotMatchException` if it appears 
            that `self` and `example` does not match (they have 
            different et of keys).
        '''
        if type(example) is not dict:
            raise NotMatchException
        
        # It is more efficient to compare key sets first.
        if set(self.attrs.keys()) != set(example.keys()):
            raise NotMatchException
        
        for key, val in example.items():
            patternval = self.attrs[key]
            depth_callback(patternval, val)
    
    def match(self, example):
        
        def match_recursion(patternval, val):
            if type(val) in (dict, list):
                if patternval is None:
                    raise NotMatchException
                
                if not patternval.match(val):
                    raise NotMatchException
            else:
                # assume generic value
                if patternval is not None:
                    raise NotMatchException

        
        try:
            self.comparison_recursion(example, match_recursion)
        except NotMatchException:
            return False
        else:
            return True
        
    def populate(self, example):
        
        array_populate_pairs = []
        
        def populate_recursion(patternval, val):
            if  type(patternval) is ArrayNode and \
                type(val) is list                 :
                    array_populate_pairs.append((patternval, val))
                    
            elif type(patternval) is DictNode and \
                 type(val) is dict                :
                    patternval.comparison_recursion(val, populate_recursion)
            elif type(val) not in (list, dict) and \
                 patternval is None:
                    pass
            else:
                raise NotMatchException
        
        self.comparison_recursion(example, populate_recursion)
        
        for patternval, val in array_populate_pairs:
            patternval.populate(val)

class ArrayNode(Node):
    def __init__(self, example):
        self.variants = []
        self.generic_values = False
        
        self.populate(example)
    
    def comparison_recursion(self, example, depth_callback,
                                            depth_end_callback,
                                            generic_callback):
        '''
            For each non-generic `val` in example
            run `depth_callback(patternval, val)` in loop for each
            (non-generic) value pattern `patternval` in `self`.
            The inner loop can be stopped by `StopIteration`
            raised by `depth_callback`.
            
            If `StopIteration` was not raised inside current inner loop,
            call `depth_end_callback(val)` for current `val`.
            
            If `val` is generic, call `generic_callback(val)`.
        '''
        if type(example) is not list:
            raise NotMatchException
        
        for val in example:
            if type(val) in (dict, list):
                end_reached = True
                for patternval in self.variants:
                    try:
                        depth_callback(patternval, val)
                    except StopIteration:
                        end_reached = False
                if end_reached:
                    depth_end_callback(val)
            else:
                generic_callback(val)
    
    def match(self, example):
        
        def match_recursion(patternval, val):
            if patternval.match(val):
                raise StopIteration
        
        def match_end_callback(val):
            raise NotMatchException
        
        def match_generic_callback(val):
            if not self.generic_values:
                raise NotMatchException
        
        try:
            self.comparison_recursion(example, match_recursion,
                                               match_end_callback,
                                               match_generic_callback)
        except NotMatchException:
            return False
        else:
            return True
    
    def populate(self, example):
        
        def populate_recursion(patternval, val):
            try:
                patternval.populate(val)
            except NotMatchException:
                pass
            else:
                raise StopIteration
        
        def populate_end_callback(val):
            self.variants.append(Node(val))
        
        def populate_generic_callback(val):
            self.generic_values = True
        
        self.comparison_recursion(example, populate_recursion,
                                           populate_end_callback,
                                           populate_generic_callback)

class GraphvizRepr(metaclass=ABCMeta):
    
    @classmethod
    def to_dot(cls, node, id_generator=None):
        '''
            Returns graphviz graph description as list of text lines
            (`str`s).
        '''
        lines = ['digraph {', 'overlap=no', '']
        root_id, root_lines = cls.to_dot_lines(node, id_generator)
        lines.extend(root_lines)
        lines.append('}')
        return lines
    
    @classmethod
    def to_dot_lines(cls, node, id_generator=None):
        '''
            Returns tuple `(node_id, edge_rows)`, where `node_id` is a 
            string identifier corresponding to this node and 
            `edge_rows` is a list of lines describing edges and 
            nodesbeneath this node.
            
            `id_generator` is expected to return unique strings or 
            numbers. This strings must not contain '"' and '\\'.
        '''
        if id_generator is None:
            id_generator = count()
        
        if type(node) is ArrayNode:
            return cls.array_to_dot_lines(node, id_generator)
        elif type(node) is DictNode:
            return cls.dict_to_dot_lines(node, id_generator)
        else:
            raise TypeError
    
    @classmethod
    @abstractmethod
    def array_to_dot_lines(cls, array, id_generator=None):
        pass
    
    @classmethod
    @abstractmethod
    def dict_to_dot_lines(cls, _dict, id_generator=None):
        pass

class TreeGraphvizRepr(GraphvizRepr):
    
    @classmethod    
    def array_to_dot_lines(cls, array, id_generator=None):
        
        id_str = 'dict_{}'.format(next(id_generator))
        lines  = []
        
        lines.append('{} [label="list", shape=diamond]' \
                     .format(id_str)            )
        
        if array.generic_values:
            generic_id = 'generic_{}'.format(next(id_generator))
            lines.append('{} [shape=point]' \
                         .format(generic_id)                )
            lines.append('{} -> {} [color=gray]'       \
                         .format(id_str, generic_id)   )
                
        for patternval in array.variants:

            child_id, child_lines = \
                cls.to_dot_lines(patternval, id_generator)
            lines.append('{} -> {} [color=gray]'     \
                         .format(id_str, child_id)   )
            lines.extend(child_lines)
        
        return id_str, lines
    
    @classmethod
    def dict_to_dot_lines(cls, _dict, id_generator=None):
        if id_generator is None:
            id_generator = count()
        
        id_str = 'dict_{}'.format(next(id_generator))
        lines  = []
        
        lines.append('{} [label="dict", shape=box]' \
                     .format(id_str)                )
        
        for key, patternval in sorted(_dict.attrs.items(), \
                                      key = lambda k: k[0]):
            if patternval is None:
                generic_id = 'generic_{}'.format(next(id_generator))
                lines.append('{} [shape=point]' \
                             .format(generic_id)                )
                lines.append('{} -> {} [label="{}"]'            \
                             .format(id_str, generic_id, key)   )
            else:
                child_id, child_lines = \
                    cls.to_dot_lines(patternval, id_generator)
                lines.append('{} -> {} [label="{}"]'            \
                             .format(id_str, child_id, key)   )
                lines.extend(child_lines)
        
        return id_str, lines

class TableGraphvizRepr(TreeGraphvizRepr):
    
    @classmethod
    def dict_to_dot_lines(cls, _dict, id_generator=None):
        if id_generator is None:
            id_generator = count()
        
        id_str = 'dict_{}'.format(next(id_generator))
        lines  = []
        
        lines.append('{} [shape=none, portPos="title:w", label=<\n'
                     '<table cellspacing="0" border="1" cellborder="0">\n'
                     '<tr><td port="title">dict</td></tr><hr/>\n'
                     '{}'
                     '</table>>]' \
                     .format(id_str,
                            '\n'.join('<tr><td align="left" port="key_{0}">{0}</td></tr>'
                                      .format(html.escape(key)) for key in sorted(_dict.attrs.keys())
                                     )
                            )
                    )
        
        for key, patternval in sorted(_dict.attrs.items(), \
                                      key = lambda k: k[0]):
            if patternval is None:
                pass
            else:
                child_id, child_lines = \
                    cls.to_dot_lines(patternval, id_generator)
                lines.append('{}:key_{key}:w -> {}'                \
                             .format(id_str, child_id, key = key)   )
                lines.extend(child_lines)
        
        return id_str, lines
        
graphviz_reprs = {'tree' : TreeGraphvizRepr,
                  'table': TableGraphvizRepr}

if __name__ == '__main__':
    import argparse as ap
    parser = ap.ArgumentParser()
    parser.add_argument('fnames', nargs='+', help='input file names')
    parser.add_argument('-r', '--repr',
                        help    = 'graphviz representation; one of: "{}"'      \
                                  .format('", "'.join(graphviz_reprs.keys())) ,
                        default = 'table')
    
    args = parser.parse_args()
    
    repr_cls = graphviz_reprs[args.repr]
    
    import json, sys
    
    root_node = None
    
    for fname in args.fnames:
        try: 
            with open(fname, 'rt') as f:
                if root_node is None:
                    root_node = Node(json.load(f))
                else:
                    root_node.populate(json.load(f))
        except Exception as e:
            print("{}: {}".format(fname, str(e)), file=sys.stderr)
    
    if root_node is not None:
        print('\n'.join(repr_cls.to_dot(root_node)))
    else:
        print('Nothing have been parsed.', file=sys.stderr)
