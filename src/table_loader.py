#    Copyright 2020 Mykhailo Mishchenko
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from collections.abc import Callable
from abc import ABCMeta, abstractmethod
from pathlib import Path
import pandas as pd
import json
from functools import partial, reduce
import warnings

class TableNotFound(Exception):
    pass

def structure_table_loader(struct):
    return pd.json_normalize(struct)

def json_table_loader(json_str):
    return structure_table_loader(json.loads(json_str))

def json_file_table_loader(json_file):
    return structure_table_loader(json.load(json_file))

def json_fname_table_loader(json_fname):
    try:
        f = Path(json_fname).open('rt')
    except FileNotFoundError:
        raise TableNotFound(json_fname)
    else:
        table = structure_table_loader(json.load(f))
        f.close()
        return table
    
class AssignProxyTableLoader(Callable):
    '''
        This class simulates a function which calls 
        `.assign(**assigners)` on newly loaded table returned from 
        `table_loader(*args, **kwargs)`.
    '''
    def __init__(self, table_loader=None, **assigners):
        if table_loader is None:
            table_loader = lambda x:x
        self.table_loader = table_loader
        self.assigners    = assigners
    def __call__(self, *args, **kwargs):
        
        return self.table_loader(*args, **kwargs) \
                   .assign(**(self.assigners))

class DatetimeProxyTableLoader(AssignProxyTableLoader):
    def __init__(self, table_loader):
        super().__init__(table_loader,
                         dateTime=lambda x: pd.to_datetime(x['dateTime']))

class HeartrateProxyTableLoader(DatetimeProxyTableLoader):
    pass

json_file_heartrate_loader = \
    HeartrateProxyTableLoader(json_file_table_loader)
json_fname_heartrate_loader = \
    HeartrateProxyTableLoader(json_fname_table_loader)
json_heartrate_loader = \
    HeartrateProxyTableLoader(json_table_loader)
structure_heartrate_loader = \
    HeartrateProxyTableLoader(structure_table_loader)

class DatabaseLoader(metaclass=ABCMeta):
    @abstractmethod
    def get_table(self, name):
        pass

class JsonDirectoryDatabaseLoader(DatabaseLoader):
    def __init__(self, path):
        ''' `path` is a path to directory, where to search for json 
            files.
        '''
        self.path = path
    
    def get_table(self, name):
        return json_fname_table_loader(
                   Path(self.path).joinpath(name + '.json')
               )

class HDF5DatabaseLoader(DatabaseLoader):
    def __init__(self, path):
        ''' `path` is a path to a HDF5 file
        '''
        self.store = pd.HDFStore(path)
    def get_table(self, name):
        try:
            return self.store[name]
        except KeyError:
            raise TableNotFound(name)

class ConvertProxyDatabaseLoader(DatabaseLoader):
    def __init__(self, real_loader, converters=None, 
                                    virtual_tables=None):
        '''
            `converters` is a `dict` with table names as keys and
            conversion functions as corresponding values.
            
            Each conversion function gets a `DataFrame` with raw table
            as input and is expected to return other `DataFrame`.
            
            `None` corresponds to a function, which will convert
            any table without related (by name) conversion function.
            
            `virtual_tables` is a dictionary where keys are
            virtual table names and corresponding values are
            `(source_table_name, virtual_creator)` tuples.
            
            If table is not available, it tries to create it from
            `source_table_name` raw wariant of table (that is, without 
            applying converters) using `virtual_creator`.
        '''
        
        self.real_loader = real_loader
        
        if converters is None:
            converters = dict()
        self.converters = converters
        
        if virtual_tables is None:
            virtual_tables = dict()
        self.virtual_tables=virtual_tables
    
    def raw_get_table(self, name, *args, **kwargs):
        try:
            return self.real_loader.get_table(name, *args, **kwargs)
        except TableNotFound:
            if name in self.virtual_tables:
                source_table_name, virtual_creator = self.virtual_tables[name]
                return virtual_creator(self.raw_get_table(source_table_name))
            else:
                raise

    def get_table(self, name, *args, **kwargs):
        
        table = self.raw_get_table(name, *args, **kwargs)
        
        if name in self.converters:
            if self.converters[name] is not None:
                return self.converters[name](table)
            else:
                return table
        elif None in self.converters:
            return self.converters[None](table)
        else:
            return table

def dateTime_converter(table, column_names = ('dateTime',)):
    ''' Convert 'dateTime' column to datetime type, if it exist.
        Else return the same table.
    '''
    
    column_names = tuple(filter(lambda key: key in table, column_names))
    
    if len(column_names) > 0:
        return table.assign(
                   **{name: (lambda n: 
                                (lambda x: pd.to_datetime(x[n]))
                            )(name)
                        for name in column_names
                     }
               )
    else:
        return table
    
def chain_functions(*funcs):
    ''' `funcs` is an iterable with one-argument functions '''
    def chained(val):
        for f in funcs:
            val = f(val)
        return val
    return chained

def del_on_object(obj, name):
    try:
        del obj[name]
    except KeyError:
        pass
    return obj

def object_column_extractor(source_table, source_column, foreign_index_column):
    '''
        Extract column from `source_table` as another table.
    '''
    return pd.concat(
        (pd.DataFrame(value).assign(**{
             foreign_index_column: lambda frame: \
                             pd.Series(row_index,
                                       index=frame.index,
                                       dtype=source_table.index.dtype)
         }) for row_index, value in source_table[source_column].dropna().iteritems()
        ),
        ignore_index = True,
        copy         = False
    )

sleep_levels_shortdata_extractor = partial(object_column_extractor,
                                           source_column='levels.shortData',
                                           foreign_index_column='sleep_index'
                                           )
sleep_levels_data_extractor      = partial(object_column_extractor,
                                           source_column='levels.data',
                                           foreign_index_column='sleep_index'
                                           )

def convert_column(table, column_name, column_converter):
    try:
        column = table[column_name]
    except KeyError:
        warnings.warn("Trying to convert missing '{}' column "
                      "to {}.".format(column_name, column_converter.__name__) )
        return table
    else:
        return table.assign(**{column_name: column_converter(column)})

column_to_categorical = partial(convert_column, column_converter = pd.Categorical)
column_to_numeric     = partial(convert_column, column_converter = pd.to_numeric)
    

class FitbitProxyLoader(ConvertProxyDatabaseLoader):
    def __init__(self, real_loader):
        
        level_to_categorical = partial(column_to_categorical,
                                       column_name = 'level' )
        convert_sleep_levels = chain_functions(dateTime_converter,
                                               level_to_categorical)
        convert_simple_timeserie = chain_functions(dateTime_converter,
                                                      partial(column_to_numeric,
                                                              column_name = 'value'))
        
        converters = {
            None   : dateTime_converter,
            'sleep': chain_functions(
                                     partial(del_on_object,
                                             name='levels.shortData'),
                                     partial(del_on_object,
                                             name='levels.data'),
                                     partial(dateTime_converter,
                                             column_names=('startTime', 'endTime'))
                                    ),
            'sleep_levels_shortdata': convert_sleep_levels,
            'sleep_levels_data'     : convert_sleep_levels,
            'distance'              : convert_simple_timeserie,
            'calories'                 : convert_simple_timeserie,
            'sedentary_minutes'        : convert_simple_timeserie,
            'lightly_active_minutes'   : convert_simple_timeserie,
            'moderately_active_minutes': convert_simple_timeserie,
            'very_active_minutes'      : convert_simple_timeserie,
            'steps'                    : convert_simple_timeserie,
            'exercise' : partial(dateTime_converter,
                                 column_names=('startTime',
                                               'originalStartTime'))
        }
        
        virtual_tables = {
            'sleep_levels_shortdata': ('sleep', sleep_levels_shortdata_extractor),
            'sleep_levels_data'     : ('sleep', sleep_levels_data_extractor)
        }
        
        super().__init__(real_loader, converters = converters,
                                      virtual_tables = virtual_tables)

class FitbitHDF5Loader(FitbitProxyLoader):
    def __init__(self, path):
        super().__init__(HDF5DatabaseLoader(path))

class FitbitJsonLoader(FitbitProxyLoader):
    def __init__(self, path):
        super().__init__(JsonDirectoryDatabaseLoader(path))

fitbit_loader_types = {
    'hdf5': FitbitHDF5Loader,
    'json': FitbitJsonLoader
}

def slice_time_range(table, date_from=None, date_to=None,
                     date_time_column='dateTime'):
    '''
        Returns rows which have a value between `date_from` and
        `date_to` in column `date_time_column`.
    '''
    return slice_time_range_overlap(table,
                                    date_from=date_from,
                                    date_to=date_to,
                                    since_column=date_time_column,
                                    until_column=date_time_column)

def time_range_overlap_mask(table, date_from=None,
                                   date_to=None,
                                   since_column=None,
                                   until_column=None ):
    skip_condition = pd.Series(False, index = table.index)
    if date_from is not None and until_column is not None:
        skip_condition = (table[until_column] < pd.Timestamp(date_from)) \
                         | skip_condition
    if date_to is not None and since_column is not None:
        skip_condition = (table[since_column] > pd.Timestamp(date_to)) \
                         | skip_condition
    
    return ~skip_condition

def time_event_overlap_mask(table, date_from=None,
                                   date_to=None,
                                   since_column=None,
                                   duration_column=None,
                                   unit='ms'            ):
    skip_condition = pd.Series(False, index = table.index)
    if date_from is not None and duration_column is not None \
                             and since_column    is not None:
        until = table[since_column] \
                + pd.to_timedelta(table[duration_column],
                                  unit=unit,
                                  errors='coerce') \
                    .fillna(pd.to_timedelta(0))
        skip_condition = (until < pd.Timestamp(date_from)) \
                         | skip_condition
    if date_to is not None and since_column is not None:
        skip_condition = (table[since_column] > pd.Timestamp(date_to)) \
                         | skip_condition
    
    return ~skip_condition
    # if not skip_condition.any():
        # return table
    # else:
        # return table[~skip_condition]

def apply_mask_func(table, mask_func, *args, **kwargs):
    mask = mask_func(table, *args, **kwargs)
    if mask.all():
        return table
    else:
        return table[mask]

slice_time_range_overlap = partial(apply_mask_func,
                                   mask_func = time_range_overlap_mask)

def or_mask_funcs(*mask_funcs):
    def ored_funcs(table):
        return reduce(lambda a, b: a | b,
                      map(lambda mf: mf(table), mask_funcs)
                     )
    return ored_funcs

class SliceTimeRangeProxyDatabaseLoader(ConvertProxyDatabaseLoader):
    def __init__(self, real_loader, date_from=None, date_to=None):
        
        converters = {None: lambda x: slice_time_range(x, date_from, date_to)}
        
        super().__init__(real_loader, converters = converters)

class SliceTimeRangeProxyFitbitLoader(ConvertProxyDatabaseLoader):
    def __init__(self, real_loader, date_from=None, date_to=None):
        
        exercise_mask_func = \
            or_mask_funcs(partial(time_event_overlap_mask,
                                  date_from=date_from,
                                  date_to=date_to,
                                  since_column='startTime',
                                  duration_column='duration'
                                 ),
                          partial(time_event_overlap_mask,
                                  date_from=date_from,
                                  date_to=date_to,
                                  since_column='originalStartTime',
                                  duration_column='originalDuration'
                                 )
                         )
        
        converters = {None   : partial(slice_time_range,
                                       date_from=date_from,
                                       date_to=date_to     ),
                      'sleep': partial(slice_time_range_overlap,
                                       date_from   =date_from,
                                       date_to     =date_to,
                                       since_column='startTime',
                                       until_column='endTime'    ),
                      'exercise': partial(apply_mask_func, 
                                          mask_func=exercise_mask_func)
                     }
        
        super().__init__(real_loader, converters = converters)
