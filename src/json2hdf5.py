#    Copyright 2020 Mykhailo Mishchenko
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


if __name__ == '__main__':
    import argparse as ap
    parser = ap.ArgumentParser()
    parser.add_argument('fnames', nargs='+', help='input file names')
    parser.add_argument('-o', help='output hdf5 file name',
                              default='store.h5')
    
    args = parser.parse_args()
    
    import pandas as pd
    import json, re
    import sys
    
    store = pd.HDFStore(args.o)
    
    for fname in args.fnames:
        try:
            with open(fname, 'rt') as f:
                table = pd.json_normalize(json.load(f))
            
            tname = re.sub(r'\.json$', '', fname)
            tname = re.sub(r'^.*/',    '', tname)
            store[tname] = table
                
        except Exception as e:
            print("{}: {}".format(fname, str(e)), file=sys.stderr)
    
    store.close()
