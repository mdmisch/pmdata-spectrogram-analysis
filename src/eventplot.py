#    Copyright 2020 Mykhailo Mishchenko
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import matplotlib.pyplot as plt
import pandas as pd
from matplotlib.dates import date2num
from mpl_toolkits.axes_grid1 import make_axes_locatable
import numpy as np
import scipy.signal as sig
from matplotlib.patches import Rectangle
from matplotlib.dates import AutoDateLocator, AutoDateFormatter
from general_detrend import general_detrend

def add_colorbar_spacing(ax):
    # Aligning solution from https://stackoverflow.com/a/54473867
    divider = make_axes_locatable(ax)
    return divider.append_axes("right", size="5%", pad=.05)

def plot_timeserie(time_series, ax, bar_ax=None, legend=None):
    # https://github.com/pandas-dev/pandas/issues/29705
    # table.plot(time_colname, value_colname, ax = ax)
    
    real_legend = []
    handles     = []
    for i, (table, time_colname, value_colname) in enumerate(time_series):
        if table is not None:
            handles.append(
                ax.plot(date2num(table[time_colname]), table[value_colname])[0]
            )
            if legend is not None:
                real_legend.append(legend[i])
    
    if bar_ax is not None:
        bar_ax.remove()
    
    if legend is not None:
        ax.legend(handles, legend, loc='upper right')

def prepare_ax(ax):
    x_locator = AutoDateLocator()
    ax.xaxis.set_major_locator(x_locator)
    ax.xaxis.set_major_formatter(AutoDateFormatter(x_locator))
    
    return (ax, add_colorbar_spacing(ax))

def plot_history(heart_rate, sleep, distance = None, calories = None,
                 sedentary_minutes = None, lightly_active_minutes = None,
                 moderately_active_minutes = None, very_active_minutes = None,
                 steps = None, exercise = None, 
                 hrNFFTs = (64, 32, 16), **kwargs):
    
    fig, axs = plt.subplots(4+len(hrNFFTs), 1, sharex=True, sharey=False)
    
    axs_iter = map(lambda ax: prepare_ax(ax), axs)
    
    # heart rate plot
    plot_timeserie(((heart_rate, 'dateTime', 'value.bpm'),), *next(axs_iter))
    
    # heart rate spectrograms
    
    bucket_len = 1
    
    hr_1min = heart_rate.set_index('dateTime') \
                        .resample('{}min'.format(bucket_len))      \
                        .mean() \
                        .interpolate()
    
    for window in hrNFFTs:
        ax, bar_ax = next(axs_iter)
        # Spectrogram is aligned in a way that a spectrum column on 
        # the image is aligned in the middle of
        # the corresponding window.
        pxx,  freq, t, cax = \
            ax.specgram(hr_1min['value.bpm'],
                             Fs=1/(60*bucket_len),
                             NFFT=window,
                             noverlap=window-1,
                             xextent=(date2num(hr_1min.index.min()) + bucket_len*window/(60*24)/2,
                                      date2num(hr_1min.index.max()) - bucket_len*window/(60*24)/2),
                             vmin=10,
                             vmax=50,
                             cmap='seismic',
                             # scale='linear'
                            )
        fig.colorbar(cax, cax=bar_ax)
        print('min_ampl={}; max_ampl={}'.format(np.nanmin(pxx), np.nanmax(pxx)))
    
    # Activity index
    # specgram = sig.spectrogram(hr_1min['value.bpm'], fs=1/(60*bucket_len),
                               # nperseg=32,
                               # noverlap=31,
                               # scaling='spectrum',
                               # detrend=general_detrend('linear'),
                               # window='boxcar')
    # specgram_log = np.log2(specgram[-1][1:])
    # m = np.array([ 1.3748116 ,  1.04000304,  0.46783426,  0.014989  , -0.36772857,
                  # -0.65069974, -0.94758337, -1.19192184, -1.41084338, -1.67842955,
                  # -1.86681222, -2.07016281, -2.2384652 , -2.3609384 , -2.41707162,
                  # -4.40295089])
    # w0 = np.arange(16)*0.013502411014661474 + -0.343395743270225

    # w0_ax, w0_bar_ax = next(axs_iter)
    # bg_patch = Rectangle((date2num(hr_1min.index.min()), 0), 
                         # width  = date2num(hr_1min.index.max()) - date2num(hr_1min.index.min()),
                         # height = 1,
                         # hatch  = '///',
                         # facecolor='lightblue',
                         # zorder=0)
    # w0_ax.add_patch(bg_patch)
    # w0_ax.imshow(   -w0[None,:] @ (specgram_log - m[:,None]),
                    # extent=(date2num(hr_1min.index.min()) + bucket_len*32/(60*24)/2,
                            # date2num(hr_1min.index.max()) - bucket_len*32/(60*24)/2,
                            # 0, 1),
                    # aspect='auto',
                    # origin='lower',
                    # cmap='seismic',
                    # # vmax and vmin chosen using 3-sigma rule.
                    # # Maybe, there is a better choice?
                    # vmax= 3*42.240556**0.5,
                    # vmin=-3*42.240556**0.5,
                    # zorder=1,
                 # )
    # # ax.set_facecolor('gray')
    # w0_ax.set_yticks([0.5])
    # w0_ax.set_yticklabels(['Activity level (32min)'])
    # w0_ax.set_ylim([0,1])
    # w0_bar_ax.remove()
    
    # Activity index #2
    
    specgram_90 = sig.spectrogram(hr_1min['value.bpm'], fs=1/(60*bucket_len),
                                  nperseg=90,
                                  noverlap=89,
                                  scaling='spectrum',
                                  detrend=general_detrend('linear'),
                                  window='boxcar')
    specgram_90_log = np.log2(specgram_90[-1][1:])
    
    m_90  = np.array([ 1.77175323,  1.49910526,  0.93197397,  0.51038581,  0.09217487,
                      -0.2109607 , -0.4791191 , -0.74087262, -0.99782617, -1.14658963,
                      -1.26692978, -1.48466258, -1.5886687 , -1.74659127, -1.85899028,
                      -1.96958493, -2.04137246, -2.22146342, -2.36258272, -2.43379809,
                      -2.57669007, -2.59753047, -2.66791345, -2.75989563, -2.82346591,
                      -2.94602266, -3.03405972, -3.1543251 , -3.22985203, -3.25802751,
                      -3.31691713, -3.44179998, -3.48638131, -3.59740687, -3.65654006,
                      -3.71269672, -3.79971203, -3.75419416, -3.87957116, -3.87745196,
                      -3.84663344, -3.83484424, -3.92536631, -3.9188736 , -5.94511506])
    w0_90 = np.arange(45)*-0.0037325322262752236 + 0.22308518259516355
    
    w1_ax, w1_bar_ax = next(axs_iter)
    bg_patch = Rectangle((date2num(hr_1min.index.min()), 0), 
                         width  = date2num(hr_1min.index.max()) - date2num(hr_1min.index.min()),
                         height = 1,
                         hatch  = '///',
                         facecolor='lightblue',
                         zorder=0)
    w1_ax.add_patch(bg_patch)
    w1_ax.imshow(   w0_90[None,:] @ (specgram_90_log - m_90[:,None]),
                    extent=(date2num(hr_1min.index.min()) + bucket_len*90/(60*24)/2,
                            date2num(hr_1min.index.max()) - bucket_len*90/(60*24)/2,
                            0, 1),
                    aspect='auto',
                    origin='lower',
                    cmap='seismic',
                    # vmax and vmin chosen using 3-sigma rule.
                    # Maybe, there is a better choice?
                    vmax= 3*80.109651**0.5,
                    vmin=-3*80.109651**0.5,
                    zorder=1,
                 )
    # ax.set_facecolor('gray')
    w1_ax.set_yticks([0.5])
    w1_ax.set_yticklabels(['Activity level (90min)'])
    w1_ax.set_ylim([0,1])
    w1_bar_ax.remove()
    
    
    
    # time period axis
    ax, bar_ax = next(axs_iter)
    
    # sleep time ranges
    
    def row_to_xranges(row):
        start = date2num(row[1]['startTime'])
        width = date2num(row[1]['endTime']) - start
        return (start, width)
    
    sleep_time = [row_to_xranges(row) for row in sleep.iterrows()]
    
    ax.broken_barh(sleep_time, (4.2,3.3),
                   color=pd.Categorical(sleep['mainSleep'], categories=(False, True)) \
                           .rename_categories(('xkcd:green','xkcd:blue'))
                  )
    
    def start_duration_to_xranges(row, start_colname, duration_colname):
        start = date2num(row[1][start_colname])
        width = row[1][duration_colname] / (24*3600*1000)
        return (start, width)
    
    exercise_time = [start_duration_to_xranges(row, 'startTime',
                                                    'duration'  ) \
                                                for row in exercise.iterrows()]
    
    ax.broken_barh(exercise_time, (1.9,1.3), facecolors='xkcd:chartreuse')
    
    original_exercise_time = \
                    [start_duration_to_xranges(row, 'originalStartTime',
                                                    'originalDuration'  ) \
                                                for row in exercise.iterrows()]
    
    ax.broken_barh(original_exercise_time, (0.5,1.3), facecolors='xkcd:chartreuse')
    
    ax.set_ylim(0, 8)
    ax.set_yticks([1.15, 2.55, 5.85])
    ax.set_yticklabels(['Exercises (original)','Exercises', 'Sleep time'])
    bar_ax.remove()
    
    # # Distance
    # plot_timeserie(((distance, 'dateTime', 'value'),), *next(axs_iter),
                                                       # legend=('distance',))
    
    # Steps
    plot_timeserie(((steps, 'dateTime', 'value'),), *next(axs_iter),
                                                       legend=('steps',))
    
    # # Calories
    # plot_timeserie(((calories, 'dateTime', 'value'),), *next(axs_iter),
                                                       # legend=('calories',))
    
    # Activity level minutes
    # plot_timeserie(((sedentary_minutes,   'dateTime', 'value'),
                    # (lightly_active_minutes, 'dateTime', 'value'),
                    # (moderately_active_minutes, 'dateTime', 'value'),
                    # (very_active_minutes, 'dateTime', 'value') ),
                   # *next(axs_iter),
                   # legend=('sedentary minutes',
                           # 'lightly_active_minutes',
                           # 'moderately_active_minutes',
                           # 'very active minutes',))
    
    # calo_ax.set_xlim(date2num(np.min(heart_rate['dateTime'])),
                     # date2num(np.max(heart_rate['dateTime'])))
    
    return fig, axs

def plot_phys_activity(tables):
    fig, axs = plt.subplots(3,1, sharex=True)
    x_locator = AutoDateLocator()
    
    for i in range(len(axs)):
        axs[i].xaxis.set_major_locator(x_locator)
    for i in range(len(axs)):
        axs[i].xaxis.set_major_formatter(AutoDateFormatter(x_locator))
    
    plot_timeserie(((tables['distance'], 'dateTime', 'value'),), axs[0],
                   legend = ('distance',))
    plot_timeserie(((tables['steps'], 'dateTime', 'value'),), axs[1],
                   legend = ('steps',))
    plot_timeserie(((tables['calories'], 'dateTime', 'value'),), axs[2],
                   legend = ('calories',))
    
    return fig, axs


if __name__ == '__main__':
    
    import argparse as ap
    from pathlib import Path
    from table_loader import fitbit_loader_types,             \
                             SliceTimeRangeProxyFitbitLoader, \
                             TableNotFound
    
    parser = ap.ArgumentParser()
    parser.add_argument('path', help='input files location',
                                type = Path,
                                nargs='?',
                                default=Path('./')          )
    parser.add_argument('--date-from', '-f',
                                       help='slice from this date',
                                       default=None          )
    parser.add_argument('--date-to'  , '-t',
                                       help='slice until this date',
                                       default=None          )
    parser.add_argument('--input-type', '-T',
                        help='type of data source; one of "{}"' \
                             .format('", "'.join(fitbit_loader_types.keys())),
                        default='hdf5',
                        choices=fitbit_loader_types.keys()
                       )
    parser.add_argument('--just-load', '-L',
                        help  ='Load data and do nothing. Intended for usage with "python -i".',
                        action='store_true'
                       )
    parser.add_argument('--hrNFFTs',
                        help   ='heart rate spectrogram NFFT',
                        nargs  ='*',
                        default=(90,),
                        type=int
                       )
    parser.add_argument('--no-hr-spec',
                        help  ='don\'t plot heartrate spectrograms',
                        action='store_const',
                        const =(),
                        dest  ='hrNFFTs'
                       )
    parser.add_argument('--walking', '-W',
                        help  ='plot distance, steps and calories',
                        action='store_true'
                       )

    args = parser.parse_args()
    
    loader = fitbit_loader_types[args.input_type](args.path)
    # add slicing by time range
    loader = SliceTimeRangeProxyFitbitLoader(loader,
                                             args.date_from,
                                             args.date_to    )
    
    def try_get_table(name):
        try:
            return loader.get_table(name)
        except TableNotFound:
            return None
    
    if not args.just_load:
        if args.walking:
            tables = {k: try_get_table(k)   for k in (#'heart_rate',
                                                      #'sleep',
                                                      'distance',
                                                      'calories',
                                                      # 'sedentary_minutes',
                                                      # 'lightly_active_minutes',
                                                      # 'moderately_active_minutes',
                                                      # 'very_active_minutes',
                                                      'steps',
                                                      #'exercise'
                                                      )}
            fig, axs = plot_phys_activity(tables)
        else:
            tables = {k: try_get_table(k)   for k in ('heart_rate',
                                                      'sleep',
                                                      # 'distance',
                                                      # 'calories',
                                                      # 'sedentary_minutes',
                                                      # 'lightly_active_minutes',
                                                      # 'moderately_active_minutes',
                                                      # 'very_active_minutes',
                                                      'steps',
                                                      'exercise')}
                                              
            fig, axs = plot_history(**tables, hrNFFTs=args.hrNFFTs)
        plt.show()
    
