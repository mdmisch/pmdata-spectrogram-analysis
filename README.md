
# Intro

This software was written as a part of a test task given to the author
when he applied for a job.

The task was to analyze the PMData dataset from
<https://datasets.simula.no/pmdata/>. This repository contains software,
developed during this research.

# Applications in this repository and how to run them

Except some small libraries, there are four applications intended to be
started from the command line:

- **struct_viewer** - a tool to visualize structure of unknown JSON file.
- **json2hdf5** - a tool to convert a collection of JSON files into
a single HDF5 file so that the data can be loaded faster in future.
- **specgram_asymm** - a tool for investigation of many different 
characteristics of hear rate's spectrogram.
- **eventplot** - a demo application which calculates and displays the 
obtained during the research activity level metric and shows how its 
values are connected with other data in the dataset.

The python code is organized in a way that all `.py` files must be seen
by the interpreter as separate libraries. This can be achieved by running
```
export PYTHONPATH="$( pwd )/src/:$PYTHONPATH"
```
in the root directory of this repository, or by running `prepate.sh`
script in the current shell as following:
```
. prepare.sh
```

Notice the dot character at the beginning of the command - it is 
significant.

**struct_viewer** and **json2hdf5** are exceptions from this - they can 
be run standalone.

# Dependencies

- **struct_viewer** has no dependencies, except standard python libraries.
- **json2hdf5**: `pandas`
- **specgram_asymm**: `pandas`, `numpy`, `matplotlib`
- **eventplot**: `pandas`, `numpy`, `matplotlib`

All code is intended to be run with Python 3.

# **struct_viewer**

This application reads a JSON file and produces a corresponding 
structure graph in graphviz format. It can process several files at 
once and build a combined graph representation if all of this files
have a JSON array at its root.

Two different forms of structure graph are implemented: "tree" and 
"table". "tree" is more basic and "table" is more compact and simple to 
understand. "table" is the default.

```
usage: python3 -m struct_viewer [-h] [-r REPR] fname [fname ...]

positional arguments:
  fnames                input file names

optional arguments:
  -h, --help            show this help message and exit
  -r REPR, --repr REPR  graphviz representation; one of: "tree", "table"
```

This program prints text representation of the graph to `stdout`, so to 
obtain graph file for `heart_rate.json` with name `heart_rate.dot`, we 
should run it as following:
```
python3 -m struct_viewer heart_rate.json > heart_rate.dot
```

Or we can create a single graph for all `heart_rate.json` files from 
the PMData dataset by running something like
```
python3 -m struct_viewer p*/fitbit/heart_rate.json > heart_rate.dot
```

This program does not depend on other code in this repository, so it 
cat also be safely run without changing the `PYTHONPATH` environment 
variable in a following way:
```
python3 struct_viewer.py heart_rate.json > heart_rate.dot
```

## Visualizing the structure graph

To visualize the graph we need the graphviz package from 
<https://graphviz.org/download/>. It is packaged for Debian, Ubuntu and 
Fedora.

We can create graph's image with following command:
```
dot -Tsvg heart_rate.dot > heart_rate.svg
```

Besides the svg image format, there are many other image formats 
implemented - see the graphviz documentation.

# **json2hdf5**

It was quickly noticed that loading the dataset directly from JSON 
files is too slow. That's why this tool was developed. It saves all 
provided `.json` files into a HDF5 file as tables with corresponding 
names.

```
usage: python3 -m json2hdf5 [-h] [-o O] fnames [fnames ...]

positional arguments:
  fnames      input file names

optional arguments:
  -h, --help  show this help message and exit
  -o O        output hdf5 file name
```

The default HDF5 file name is `store.h5`. It can be changed with `-o`
command line option.

So, all data from single participant from the PMData dataset can be 
converted with following command:
```
python3 -m json2hdf5 *.json
```

# **specgram_asymm**

```
usage: python3 -m specgram_asymm [-h] [--date-from DATE_FROM] [--date-to DATE_TO]
                                 [--input-type {hdf5,json}]
                                 [--plot-type {histogram,scatter3d,scatter,pca_histogram,pca_scatter3d,pca_spectrogram,pca,pca_mean_dir,pca_scatter,pca_components}]
                                 [--just-load] [--nperseg NPERSEG]
                                 [--noverlap NOVERLAP] [--scaling SCALING]
                                 [--log {e,10,2,sqrt}] [--bins BINS] [-x X] [-y Y]
                                 [-z Z] [--pc] [--pca-pc] [--pca-on-spectrum]
                                 [--no-pca-on-spectrum] [--no-pc] [--whiten]
                                 [--cmap CMAP] [--bucket-len BUCKET_LEN] [-d DETREND]
                                 [-c COLOR]
                                 [--hist-to-plot [HIST_TO_PLOT [HIST_TO_PLOT ...]]]
                                 [--pca-components-other]
                                 [path]

positional arguments:
  path                  input files location

optional arguments:
  -h, --help            show this help message and exit
  --date-from DATE_FROM, -f DATE_FROM
                        slice from this date
  --date-to DATE_TO, -t DATE_TO
                        slice until this date
  --input-type {hdf5,json}, -T {hdf5,json}
                        type of data source; one of "hdf5", "json"
  --plot-type {histogram,scatter3d,scatter,pca_histogram,pca_scatter3d,pca_spectrogram,pca,pca_mean_dir,pca_scatter,pca_components}, -p {histogram,scatter3d,scatter,pca_histogram,pca_scatter3d,pca_spectrogram,pca,pca_mean_dir,pca_scatter,pca_components}
                        type of plot to build
  --just-load, -L       Load data and do nothing. Intended for usage with
                        "python -i".
  --nperseg NPERSEG     heart rate spectrogram window
  --noverlap NOVERLAP   heart rate spectrogram window overlap
  --scaling SCALING, -S SCALING
                        scaling in scipy.signal.spectrogram
  --log {e,10,2,sqrt}, -l {e,10,2,sqrt}
                        apply logarithm or other scaling
  --bins BINS, -b BINS  histogram bins
  -x X                  scatter plot x axis
  -y Y                  scatter plot y axis
  -z Z                  scatter plot z axis
  --pc                  compute probability characteristics
  --pca-pc              compute probability characteristics of PCA results
  --pca-on-spectrum     compute PCA only on spectrum, excluding means (this is
                        the default)
  --no-pca-on-spectrum  compute PCA on spectrum and means
  --no-pc               don't compute probability characteristics
  --whiten              whiten PCA
  --cmap CMAP
  --bucket-len BUCKET_LEN
                        length to upsample heartrate in minutes
  -d DETREND, --detrend DETREND
                        detrend alg.
  -c COLOR, --color COLOR
                        color used for scatter plots
  --hist-to-plot [HIST_TO_PLOT [HIST_TO_PLOT ...]]
                        plot only histograms corresponding to this numbers
                        (numeration starts from zero)
  --pca-components-other
                        plot vectors for component 1 and 2 in 'pca-components'
                        plot type
```

This is a tool written to research heart rate spectrogram's 
characteristics. It can do principal component analysis (PCA) on the 
spectrogram and draw many different kind of plots. A particular type of 
plot is choosen with `--plot-type` command line argument. "pca" 
pseudo plot type just calculates PCA and don't plot anything.

A dataset can be loaded from HDF5 or JSON storage. A particular storage 
type is choosen with `--input-type` command line argument.

## Loading from different file formats

When loading 
from JSON, you need to start this program in directory with a 
`heart_rate_json` file, or provide the `path` command line argument with
directory containing this file. For example:
```
python3 -m specgram_asymm --plot-type scatter --input-type json p14/fitbit/
```


When loading from HDF5 file, you need to give path to this file as 
`path`. For example:
```
python3 -m specgram_asymm --plot-type scatter shore.h5
```

"hdf5" input type it the default.

# **eventplot**

This program is intended as a demonstration of the obtained metric for 
activity level. Also, with `--walking` command line argument it 
produces a plot which shows a delay in calories' data.

`--input-type` and `path` command line arguments are the same, as in
**specgram_asymm**.

```
python3 -m eventplot [-h] [--date-from DATE_FROM] [--date-to DATE_TO]
                     [--input-type {hdf5,json}] [--just-load]
                     [--hrNFFTs [HRNFFTS [HRNFFTS ...]]] [--no-hr-spec]
                     [--walking]
                     [path]

positional arguments:
  path                  input files location

optional arguments:
  -h, --help            show this help message and exit
  --date-from DATE_FROM, -f DATE_FROM
                        slice from this date
  --date-to DATE_TO, -t DATE_TO
                        slice until this date
  --input-type {hdf5,json}, -T {hdf5,json}
                        type of data source; one of "hdf5", "json"
  --just-load, -L       Load data and do nothing. Intended for usage with
                        "python -i".
  --hrNFFTs [HRNFFTS [HRNFFTS ...]]
                        heart rate spectrogram NFFT
  --no-hr-spec          don't plot heartrate spectrograms
  --walking, -W         plot distance, steps and calories
```

# LICENSE

The software this README is attached to is free software: you can 
redistribute it and/or modify it under the terms of the GNU General 
Public License as published by the Free Software Foundation, either 
version 3 of the License, or (at your option) any later version.

The software this README is attached to is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
